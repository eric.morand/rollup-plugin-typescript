import type {Plugin} from "rollup";
import {TransformResult} from "rollup";
import typeScript from "typescript";
import {createFilter} from "@rollup/pluginutils";
import {normalize, resolve} from "path";
import {CompilationResult, createCompiler} from "./compiler";
import {Volume} from "memfs";
import {isADeclarationOutputFile} from "./helpers";

export type FactoryOptions = {
    /**
     * An instance of TypeScript `CompilerOptions`, minus the properties `inlineSourceMap` and `sourceMap`. Used by the factory to either override the compiler options resolved from the first available `tsconfig.json` file (starting from the current working directory) if any, or as the entire set of compiler options otherwise.
     *
     * Note that the `inlineSourceMap` and `sourceMap` properties will always be passed as `false` and `true`, respectively, to the underlying TypeScript compiler, in order to guarantee that the plugin is always capable of returning a source map to the Rollup engine.
     */
    compilerOptions?: Omit<typeScript.CompilerOptions, "inlineSourceMap" | "sourceMap">;

    /**
     * A pattern, or an array of patterns, which specifies the files in the build the plugin should ignore. By default, no files are ignored.
     */
    exclude?: ReadonlyArray<string | RegExp> | string | RegExp;

    /**
     * A pattern, or an array of patterns, which specifies the files in the build the plugin should operate on. By default, `.ts`, `.cts`, `.mts` and `.tsx` files are targeted.
     */
    include?: ReadonlyArray<string | RegExp> | string | RegExp;
};

/**
 * Returns a newly created instance of [Plugin](https://rollupjs.org/plugin-development/) from the passed options.
 *
 * @param options
 */
export const createTypeScriptPlugin = (options: FactoryOptions = {}) => {
    let passedCompilerOptions = options.compilerOptions || {};
    
    const configFilePath = typeScript.findConfigFile(process.cwd(), typeScript.sys.fileExists);

    const tsConfigFile: {
        compilerOptions: typeScript.CompilerOptions
    } = configFilePath ? typeScript.readConfigFile(configFilePath, typeScript.sys.readFile).config : {};
    
    const parsedCommandLine = typeScript.parseJsonConfigFileContent(
        {
            ...tsConfigFile,
            compilerOptions: {
                ...tsConfigFile.compilerOptions,
                ...passedCompilerOptions
            }
        },
        typeScript.sys,
        process.cwd()
    );
    
    const compilerOptions: typeScript.CompilerOptions = {
        ...parsedCommandLine.options,
        inlineSourceMap: false,
        sourceMap: true
    };
    
    const fileSystem = new Volume();
    const compiler = createCompiler(compilerOptions, fileSystem);

    const createModuleResolver = () => {
        const cache = typeScript.createModuleResolutionCache(
            process.cwd(),
            host.getCanonicalFileName,
            compilerOptions
        );

        return (moduleName: string, containingFile: string, redirectedReference?: typeScript.ResolvedProjectReference, mode?: typeScript.ResolutionMode) => {
            const {resolvedModule} = typeScript.resolveModuleName(
                moduleName,
                containingFile,
                compilerOptions,
                typeScript.sys,
                cache,
                redirectedReference,
                mode
            );

            return resolvedModule;
        };
    };

    const host = typeScript.createCompilerHost(compilerOptions);

    // options
    // @see https://github.com/rollup/plugins/issues/1651#issuecomment-1868495405
    options.include = options.include || /\.(cts|mts|ts|tsx)$/;

    if (typeof options.include === "string") {
        options.include = [options.include];
    }

    if (Array.isArray(options.include)) {
        options.include = options.include.map((include) => {
            if (typeof include === "string") {
                return new RegExp(include);
            }

            return include;
        });
    }

    const filter = createFilter(options.include, options.exclude);
    const resolveModule = createModuleResolver();

    let compilationResult: CompilationResult | null;

    const plugin: Plugin<null> = {
        name: 'typescript',

        buildStart() {
            compilationResult = null;
        },

        transform(_code, id): TransformResult {
            if (!filter(id)) {
                return null;
            }

            const resolvedId = resolve(id);

            if (compilationResult === null) {
                compilationResult = compiler.compile(resolvedId);
            }

            const errorsToEmit = compilationResult.filter((error) => {
                // if error is a string, this is a global error
                return typeof error === "string" || error.file === resolvedId;
            });

            if (errorsToEmit.length > 0) {
                for (const error of errorsToEmit) {
                    if (typeof error === "string") {
                        this.error(error);
                    }
                    else {
                        const {column, line, message} = error;

                        this.error(message, {
                            column,
                            line
                        });
                    }
                }
            }

            const outputFileNames = compiler.getOutputFileNames(resolvedId);

            const code = fileSystem.existsSync(outputFileNames.code) && fileSystem.readFileSync(outputFileNames.code);

            if (code) {
                const map = fileSystem.existsSync(outputFileNames.code) && fileSystem.readFileSync(outputFileNames.map)!;

                return {
                    code: code.toString(),
                    map: map.toString()
                };
            }

            return null;
        },
        resolveId(importee, importer) {
            if (importer !== undefined) {
                const containingFile = normalize(importer);

                const mode = typeScript.getImpliedNodeFormatForFile(
                    containingFile as any, // todo: https://github.com/microsoft/TypeScript/issues/56852
                    undefined,
                    typeScript.sys,
                    compilerOptions
                );

                const resolved = resolveModule(
                    importee,
                    containingFile,
                    undefined,
                    mode
                )!;

                if (isADeclarationOutputFile(resolved.resolvedFileName)) {
                    return null;
                }

                return normalize(resolve(resolved.resolvedFileName));
            }

            return null;
        }
    };

    return plugin;
};

export default createTypeScriptPlugin;