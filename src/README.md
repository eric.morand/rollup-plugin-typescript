# @nightlycommit/rollup-plugin-typescript
[![NPM version][npm-image]][npm-url] [![Build Status][build-image]][build-url] [![Coverage percentage][coveralls-image]][coveralls-url]

A Rollup plugin for seamless integration between Rollup and TypeScript.

## Why another TypeScript plugin?

This plugin was written because none of the two main TypeScript plugins available for Rollup are satisfactory:

* `@rollup/plugin-typescript`:
  * Watches files and triggers re-build by itself, meaning that it conflicts with any development tool that is _not_ Rollup. See [this issue](https://github.com/rollup/plugins/issues/1656) for more information.
  * Doesn't work consistently depending on _where_ Rollup is executed, meaning that it can't be used in any other context than the opinionated one that it was written with in mind. See [this issue](https://github.com/rollup/plugins/issues/1651) for more information.
  * Doesn't work at all as soon as there is a source file in the current working directory that is not valid, even if it is _not_ part of the dependency graph of the entry point. See [this issue](https://github.com/rollup/plugins/issues/1652) for more information.
  * Is opinionated instead of honoring TypeScript compiler options as they are passed, basically babysitting developers instead of considering that they know what they are doing.

* `rollup-plugin-typescript2`:
  * Doesn't work at all as soon as there is a source file in the current working directory that is not valid, even if it is _not_ part of the dependency graph of the entry point.
  * Is opinionated instead of honoring TypeScript compiler options as they are passed, basically babysitting developers instead of considering that they know what they are doing.

Additionally, this plugin is substantially faster than both the two main alternatives thanks to its native support of the incremental feature of TypeScript compiler and the reliance on the canonical resolution algorithm of the TypeScript compiler that considers _only_ the files that are part of the entry point dependency graph.

## Requirements

This plugin requires at least [Rollup](https://www.npmjs.com/package/rollup) 4.9.

## Installation

The recommended way to install the package is via npm:

```shell
npm install @nightlycommit/rollup-plugin-typescript --save-dev
```

## Usage

### API

Create a Rollup configuration file, import the plugin factory and add an instance to the list of `plugins`:

```js
// rollup.config.mjs
import {createTypeScriptPlugin} from '@nightlycommit/rollup-plugin-typescript';

export default {
    input: 'index.ts',
    output: {
        dir: 'output'
    },
    plugins: [
        createTypeScriptPlugin()
    ]
};
```

### CLI

```shell
rollup -i index.ts -o index.js -p @nightlycommit/rollup-plugin-typescript
```

## Plugin factory signature

```typescript
import type {CompilerOptions} from "typescript";
import type {Rollup} from "rollup";

type PluginFactory = (options?: {
    compilerOptions?: CompilerOptions;
    exclude?: ReadonlyArray<string | RegExp> | string | RegExp;
    include?: ReadonlyArray<string | RegExp> | string | RegExp;
}) => Plugin<null>;
```

### options

#### compilerOptions

An instance of TypeScript `CompilerOptions`, minus the properties `inlineSourceMap` and `sourceMap`. Used by the factory to either override the compiler options resolved from the first available `tsconfig.json` file (starting from the current working directory) if any, or as the entire set of compiler options otherwise.

Note that the `inlineSourceMap` and `sourceMap` properties will always be passed as `false` and `true`, respectively, to the underlying TypeScript compiler, in order to guarantee that the plugin is always capable of returning a source map to the Rollup engine.

#### exclude

A pattern, or an array of patterns, which specifies the files in the build the plugin should ignore. By default, no files are ignored.

#### include

A pattern, or an array of patterns, which specifies the files in the build the plugin should operate on. By default, `.ts`, `.cts`, `.mts` and `.tsx` files are targeted.

[npm-image]: https://badge.fury.io/js/@nightlycommit%2Frollup-plugin-typescript.svg
[npm-url]: https://npmjs.org/package/@nightlycommit/rollup-plugin-typescript
[build-image]: https://gitlab.com/nightlycommit/rollup-plugin-typescript/badges/main/pipeline.svg
[build-url]: https://gitlab.com/nightlycommit/rollup-plugin-typescript/-/pipelines
[coveralls-image]: https://coveralls.io/repos/gitlab/nightlycommit/rollup-plugin-typescript/badge.svg
[coveralls-url]: https://coveralls.io/gitlab/nightlycommit/rollup-plugin-typescript
