import {Plugin, rollup, RollupError} from "rollup";
import {join, dirname} from "path";
import {restore, stub} from "sinon";
import {createTypeScriptPlugin, FactoryOptions} from "../../src/lib/plugin";
import {Test} from "tape";
import {Volume} from "memfs";
import ts from "typescript";

export const createFilesystemPlugin = (
    volume: InstanceType<typeof Volume>
): Plugin => {
    return {
        name: 'filesystem',
        resolveId: (importee, importer) => {
            const resolvedId = importer ? join(dirname(importer), importee) : importee;

            if (volume.existsSync(resolvedId)) {
                return resolvedId;
            }

            return null;
        },
        load: (id) => {
            return volume.readFileSync(id).toString();
        }
    };
};

export const evaluate = (code: string): any => {
    const evaluator = new Function('require', `exports = {};
${code}

return exports;
`);

    return evaluator(require);
};

const stubTsSys = (volume: InstanceType<typeof Volume>) => {
    const directoryExists = ts.sys.directoryExists;
    const fileExists = ts.sys.fileExists;
    const readFile = ts.sys.readFile;

    const isFile = (path: string) => volume.statSync(path, {
        throwIfNoEntry: false
    })?.isFile() || false;

    stub(ts.sys, "readDirectory").callsFake((path) => {
        const results: Array<string> = [];

        const stack: Array<string> = [
            path
        ];

        while (stack.length > 0) {
            const path = stack.pop() as string;
            const entries = volume.readdirSync(path).map((file) => {
                return join(path, file.toString());
            });

            for (const entry of entries) {
                const isDirectory = volume.statSync(entry).isDirectory();

                if (!isDirectory) {
                    results.push(entry);
                }
                else {
                    stack.push(entry);
                }
            }
        }

        return results;
    });

    stub(ts.sys, "directoryExists").callsFake((path) => {
        return volume.statSync(path, {
            throwIfNoEntry: false
        })?.isDirectory() || directoryExists(path);
    });

    stub(ts.sys, "readFile").callsFake((path) => {
        return (isFile(path) && volume.readFileSync(path).toString()) || readFile(path);
    });

    stub(ts.sys, "fileExists").callsFake((path) => {
        // we don't want the project tsconfig.json to be considered as part of the file system available to the tests
        // ideally, the whole file system would be virtualized, but this is too much a hassle to virtualize the whole
        // node_modules for no sensible benefit
        if (path.endsWith('tsconfig.json')) {
            return isFile(path);
        }

        return isFile(path) || fileExists(path);
    });
};

export const runTest = (
    test: Test,
    entry: string,
    fileSystem: Record<string, string>,
    definition: {
        options?: FactoryOptions;
        expectation?: any;
        expectedCompilationError?: {
            loc: RollupError["loc"];
            message: string;
            plugin: string | undefined;
        };
        expectedRuntimeError?: {
            message: string | {
                startsWith: string;
            };
        };
        fileSystem?: Record<string, string>;
        volume?: InstanceType<typeof Volume>;
    }
) => {
    const volume = definition.volume || Volume.fromJSON(fileSystem);

    stubTsSys(volume);

    const {same, end, fail} = test;

    const {expectation, expectedCompilationError, expectedRuntimeError, options} = definition;

    const plugin = createTypeScriptPlugin(options);
    
    return rollup({
        input: entry,
        plugins: [
            createFilesystemPlugin(volume),
            plugin
        ]
    }).then((build) => {
        return build.generate({
            format: "cjs"
        })
    }).then((output) => {
        const {code} = output.output[0];
        
        try {
            const module = evaluate(code);

            if (expectation !== undefined) {
                same(module, expectation);
            }
            else {
                fail(`should throw ${expectedRuntimeError}`);
            }
        } catch (error) {
            if (expectedRuntimeError !== undefined) {
                const {message} = expectedRuntimeError;
                
                if (typeof message === "string") {
                    same((error as Error).message, message);
                }
                else {
                    same((error as Error).message.startsWith(message.startsWith), true);
                }
            }
            else {
                fail((error as Error).message);
            }
        }
    }).catch((error) => {
        if (expectedCompilationError !== undefined) {
            const {loc, message, plugin} = error as RollupError;

            same(loc, expectedCompilationError.loc);
            same(message, expectedCompilationError.message);
            same(plugin, expectedCompilationError.plugin);
        }
        else {
            fail((error as Error).message);
        }
    }).finally(() => {
        restore();

        end();
    });
};