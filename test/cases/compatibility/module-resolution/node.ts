import tape from "tape";

import {runTest} from "../../../helpers/index";

tape('honors Node module resolution', ({test}) => {
    test('through direct access', (test) => {
        return runTest(test, 'entry.ts', {
            'entry.ts': `export {foo} from "./foo";`,
            'foo.ts': `export const foo: number = 5;`,
            'tsconfig.json': JSON.stringify({
                compilerOptions: {
                    moduleResolution: 'Node',
                    target: 'ES2015'
                }
            })
        }, {
            expectation: {
                foo: 5
            }
        });
    });

    test('through the main directive', (test) => {
        return runTest(test, 'entry.ts', {
            'entry.ts': `export {foo} from "foo";`,
            'node_modules/foo/package.json': JSON.stringify({
                main: `./dist/index.js`
            }),
            'node_modules/foo/dist/index.js': `export const foo = 5;`,
            'tsconfig.json': JSON.stringify({
                compilerOptions: {
                    moduleResolution: 'Node',
                    target: 'ES2015'
                }
            }),
        }, {
            expectation: {
                foo: 5
            }
        });
    });
});