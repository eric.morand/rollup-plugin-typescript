import tape from "tape";

import {runTest} from "../../../helpers/index";

tape('honors Classic module resolution', ({test}) => {
    test('through direct access', (test) => {
        return runTest(test, 'entry.ts', {
            'entry.ts': `export {foo} from "./foo";`,
            'foo.ts': `export const foo: number = 5;`,
            'tsconfig.json': JSON.stringify({
                compilerOptions: {
                    moduleResolution: 'Classic',
                    target: 'ES2015'
                }
            })
        }, {
            expectation: {
                foo: 5
            }
        });
    });
});