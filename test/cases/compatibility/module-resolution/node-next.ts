import tape from "tape";
import {runTest} from "../../../helpers/index";
import {resolve} from "path";

tape('honors NodeNext module resolution', ({test}) => {
    test('throws an error on not compatible module compiler option', (test) => {
        runTest(test, 'entry.ts', {
            'entry.ts': 'export const foo: number = 5;',
            'tsconfig.json': JSON.stringify({
                compilerOptions: {
                    noEmitOnError: true,
                    module: 'ESNext',
                    moduleResolution: 'NodeNext'
                }
            })
        }, {
            expectedCompilationError: {
                loc: undefined,
                message: `Option 'module' must be set to 'NodeNext' when option 'moduleResolution' is set to 'NodeNext'.`,
                plugin: 'typescript'
            }
        });
    });

    test('resolves dependencies successfully', ({test}) => {
        test('through the main directive', (test) => {
            return runTest(test, 'entry.mts', {
                'entry.mts': `export {foo} from "foo";`,
                'node_modules/foo/package.json': JSON.stringify({
                    main: `index.mjs`
                }),
                'node_modules/foo/index.mjs': `export const foo = 5;`,
                'tsconfig.json': JSON.stringify({
                    compilerOptions: {
                        module: 'NodeNext',
                        moduleResolution: 'NodeNext'
                    }
                })
            }, {
                expectation: {
                    foo: 5
                }
            });
        });

        test('through the exports directive', (test) => {
            return runTest(test, 'entry.mts', {
                'entry.mts': `export {foo} from "foo";`,
                'node_modules/foo/package.json': JSON.stringify({
                    exports: {
                        'default': `./dist/index.mjs`
                    }
                }),
                'node_modules/foo/dist/index.mjs': `export const foo = 5;`,
                'tsconfig.json': JSON.stringify({
                    compilerOptions: {
                        module: 'NodeNext',
                        moduleResolution: 'NodeNext'
                    }
                })
            }, {
                expectation: {
                    foo: 5
                }
            });
        });
    });

    test('emits error found in dependency', ({test}) => {
        test('through the main directive', (test) => {
            return runTest(test, 'entry.mts', {
                'entry.mts': `export {foo} from "foo";`,
                'node_modules/foo/package.json': JSON.stringify({
                    main: `index.mts`
                }),
                'node_modules/foo/index.mts': `export const foo: number = "5"; export const bar: string = 5;`,
                'tsconfig.json': JSON.stringify({
                    compilerOptions: {
                        noEmitOnError: true,
                        module: 'NodeNext',
                        moduleResolution: 'NodeNext'
                    }
                })
            }, {
                expectedCompilationError: {
                    loc: {
                        column: 13,
                        line: 1,
                        file: resolve('node_modules/foo/index.mts')
                    },
                    message: `Type 'string' is not assignable to type 'number'.`,
                    plugin: 'typescript'
                }
            });
        });

        test('through the exports directive', (test) => {
            return runTest(test, 'entry.mts', {
                'entry.mts': `export {foo} from "foo";`,
                'node_modules/foo/package.json': JSON.stringify({
                    exports: {
                        'default': `./dist/index.mts`
                    }
                }),
                'node_modules/foo/dist/index.mts': `export const foo: number = "5";`,
                'tsconfig.json': JSON.stringify({
                    compilerOptions: {
                        noEmitOnError: true,
                        module: 'NodeNext',
                        moduleResolution: 'NodeNext'
                    }
                })
            }, {
                expectedCompilationError: {
                    loc: {
                        column: 13,
                        line: 1,
                        file: resolve('node_modules/foo/dist/index.mts')
                    },
                    message: `Type 'string' is not assignable to type 'number'.`,
                    plugin: 'typescript'
                }
            });
        });
    });
});