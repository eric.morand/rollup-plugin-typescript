import tape from "tape";

import {runTest} from "../../../helpers/index";

tape('honors Node16 module resolution', ({test}) => {
    test('throws an error on not compatible module', (test) => {
        runTest(test, 'entry.ts', {
            'entry.ts': 'export const foo: number = 5;',
            'tsconfig.json': JSON.stringify({
                compilerOptions: {
                    moduleResolution: 'Node16'
                }
            })
        }, {
            expectedCompilationError: {
                loc: undefined,
                message: `Option 'module' must be set to 'Node16' when option 'moduleResolution' is set to 'Node16'.`,
                plugin: 'typescript'
            }
        });
    });

    test('resolves dependencies successfully', ({test}) => {
        test('through the main directive', (test) => {
            return runTest(test, 'entry.mts', {
                'entry.mts': `export {foo} from "foo";`,
                'node_modules/foo/package.json': JSON.stringify({
                    main: `index.mjs`
                }),
                'node_modules/foo/index.mjs': `export const foo = 5;`,
                'tsconfig.json': JSON.stringify({
                    compilerOptions: {
                        module: 'Node16',
                        moduleResolution: 'Node16'
                    }
                })
            }, {
                expectation: {
                    foo: 5
                }
            });
        });

        test('through the exports directive', (test) => {
            return runTest(test, 'entry.mts', {
                'entry.mts': `export {foo} from "foo";`,
                'node_modules/foo/package.json': JSON.stringify({
                    exports: {
                        'default': `./dist/index.mjs`
                    }
                }),
                'node_modules/foo/dist/index.mjs': `export const foo = 5;`,
                'tsconfig.json': JSON.stringify({
                    compilerOptions: {
                        module: 'Node16',
                        moduleResolution: 'Node16'
                    }
                })
            }, {
                expectation: {
                    foo: 5
                }
            });
        });
    });
});