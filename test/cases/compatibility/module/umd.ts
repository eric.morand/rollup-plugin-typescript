import tape from "tape";
import {runTest} from "../../../helpers/index";

tape('UMD module compatibility', (test) => {
    return runTest(test, 'entry.ts', {
        'entry.ts': 'export const foo: number = 5;',
        'tsconfig.json': JSON.stringify({
            compilerOptions: {
                module: 'UMD'
            }
        })
    }, {
        expectation: {}
    });
});