import tape from "tape";
import {runTest} from "../../../helpers/index";

tape('ESNext module compatibility', (test) => {
    return runTest(test, 'entry.ts', {
        'entry.ts': 'export const foo: number = 5;',
        'tsconfig.json': JSON.stringify({
            compilerOptions: {
                module: 'ESNext'
            }
        })
    }, {
        expectation: {
            foo: 5
        }
    });
});