import tape from "tape";
import {runTest} from "../../../helpers/index";

tape('System module compatibility', (test) => {
    return runTest(test, 'entry.ts', {
        'entry.ts': 'export const foo: number = 5;',
        'tsconfig.json': JSON.stringify({
            compilerOptions: {
                module: 'System'
            }
        })
    }, {
        expectedRuntimeError: {
            message: 'System is not defined'
        }
    });
});