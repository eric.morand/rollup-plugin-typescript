import tape from "tape";
import {runTest} from "../../../helpers/index";

tape('None module compatibility', (test) => {
    return runTest(test, 'entry.ts', {
        'entry.ts': 'export const foo: number = 5;',
        'tsconfig.json': JSON.stringify({
            compilerOptions: {
                noEmitOnError: true,
                module: 'None'
            }
        })
    }, {
        expectedCompilationError: {
            loc: {
                column: 0,
                file: 'entry.ts',
                line: 1
            },
            message: `Cannot use imports, exports, or module augmentations when '--module' is 'none'.`,
            plugin: 'typescript'
        }
    });
});