import tape from "tape";
import {runTest} from "../../../helpers/index";

tape('CommonJS module compatibility', (test) => {
    return runTest(test, 'entry.ts', {
        'entry.ts': 'export const foo: number = 5;',
        'tsconfig.json': JSON.stringify({
            compilerOptions: {
                module: 'CommonJS'
            }
        })
    }, {
        expectation: {
            foo: 5
        }
    });
});