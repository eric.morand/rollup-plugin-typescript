import tape from "tape";
import {createCompiler} from "../../../src/lib/compiler";
import {Volume} from "memfs";
import {spy} from "sinon";
import {resolve} from "path";

tape('compiler honors incremental', ({same, end}) => {
    const volume = Volume.fromJSON({
        'entry.ts': `export {foo} from "./foo";
export {bar} from "./bar";      
`,
        'foo.ts': `import {bar} from "./bar";
export const foo: number = 5 + bar;`,
        'bar.ts': 'export const bar: number = 5;'
    });

    const compiler = createCompiler({
        incremental: true,
        tsBuildInfoFile: '.tsbuildinfo',
        noEmitOnError: true
    }, volume);

    const writeFileSpy = spy(volume, "writeFileSync");

    compiler.compile('entry.ts');
    
    same(writeFileSpy.getCalls().map((call) => call.firstArg), [
        resolve('bar.js'),
        resolve('foo.js'),
        'entry.js',
        '.tsbuildinfo'
    ]);

    volume.writeFileSync('entry.ts', `export {foo} from "./foo";
export {bar} from "./bar";
export const oof: number = 15;      
`);

    writeFileSpy.resetHistory();

    compiler.compile('entry.ts');

    // at that point, the only way we have to guarantee that incremental compilation happened
    // is to check the order of writeFileSync calls: on incremental compilation, the entry point
    // - which is the only one that was changed - is written first
    same(writeFileSpy.getCalls().map((call) => call.firstArg), [
        'entry.js',
        resolve('bar.js'),
        resolve('foo.js'),
        '.tsbuildinfo'
    ]);

    end();
});