import tape from "tape";
import * as index from "../../src/lib";

tape('Public API', ({same, end}) => {
    const expected: Array<string> = [
        'default',
        'createTypeScriptPlugin'
    ];

    const propertyNames = Object.getOwnPropertyNames(index).filter((name) => name !== '__esModule');

    for (const key of expected) {
        same(propertyNames.includes(key), true, `${key} is exported by the index`);
    }

    for (const key of propertyNames) {
        same(expected.includes(key), true, `${key} is legit in the index`);
    }

    end();
});