import tape from "tape";
import {runTest} from "../helpers/index";

tape('resolve', ({test}) => {
    test('filters out declaration files', (test) => {
        return runTest(test, 'entry.ts', {
            'entry.ts': `import {foo} from "foo";
export const bar = {
    foo
};
`,
            'node_modules/foo/package.json': JSON.stringify({
                types: './lib.d.ts'
            }),
            'node_modules/foo/lib.d.ts': `export const foo: number = 5;`,
            'tsconfig.json': JSON.stringify({
                compilerOptions: {
                    module: "ESNext",
                    moduleResolution: 'node'
                }
            })
        }, {
            expectedRuntimeError: {
                message: {
                    startsWith: `Cannot find module 'foo'`
                }
            }
        });
    });
})