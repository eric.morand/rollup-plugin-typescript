import tape from "tape";
import {runTest} from "../helpers/index";
import {resolve} from "path";

tape('error', ({test}) => {
    test('emits error found in the entry file', (test) => {
        return runTest(test, 'entry.ts', {
            'entry.ts': 'const foo: number = "5";'
        }, {
            options: {
                compilerOptions: {
                    noEmitOnError: true
                }  
            },
            expectedCompilationError: {
                loc: {
                    column: 6,
                    file: 'entry.ts',
                    line: 1
                },
                message: `Type 'string' is not assignable to type 'number'.`,
                plugin: 'typescript'
            }
        });
    });

    test('emits error found in a dependency', (test) => {
        return runTest(test, 'entry.ts', {
            'entry.ts': 'export {foo} from "./foo";',
            'foo.ts': 'export const foo: number = "5";',
            'tsconfig.json': JSON.stringify({
                compilerOptions: {
                    moduleResolution: 'Node',
                    target: 'ES2015'
                }
            })
        }, {
            options: {
                compilerOptions: {
                    noEmitOnError: true
                }
            },
            expectedCompilationError: {
                loc: {
                    column: 13,
                    file: resolve('foo.ts'),
                    line: 1
                },
                message: `Type 'string' is not assignable to type 'number'.`,
                plugin: 'typescript'
            }
        });
    });

    test('emits error on missing entry', (test) => {
        return runTest(test, 'entry.ts', {
            'tsconfig.json': JSON.stringify({
                noEmitOnError: false
            })
        }, {
            expectedCompilationError: {
                loc: undefined,
                message: `Could not resolve entry module "entry.ts".`,
                plugin: undefined
            }
        });
    });
});