import tape from "tape";
import {runTest} from "../helpers/index";
import {resolve} from "path";

tape('options', ({test}) => {
    test('sensible default', (test) => { // todo: rename title
        return runTest(test, 'entry.ts', {
            'entry.mts': `export const foo: number = 5;`
        }, {
            expectedCompilationError: {
                loc: undefined,
                message: 'Could not resolve entry module "entry.ts".',
                plugin: undefined
            }
        });
    });

    test('honors include options', (test) => {
        return runTest(test, 'entry.ts', {
            'entry.mts': `export const foo: number = 5;`
        }, {
            options: {
                include: /\.foo$/
            },
            expectedCompilationError: {
                loc: undefined,
                message: 'Could not resolve entry module "entry.ts".',
                plugin: undefined
            }
        });
    });

    test('honors include options with dependencies', (test) => {
        return runTest(test, 'entry.ts', {
            'entry.ts': `export {foo} from "./foo";`,
            'foo.tsx': `export const foo: number = 5;`,
            'tsconfig.json': JSON.stringify({
                compilerOptions: {
                    jsx: 'react',
                    target: 'ES2015'
                }
            })
        }, {
            options: {
                include: /\.ts$/
            },
            expectedCompilationError: {
                loc: {
                    column: 13,
                    file: resolve('foo.tsx'),
                    line: 1
                },
                message: `'const' declarations must be initialized (Note that you need plugins to import files that are not JavaScript)`,
                plugin: undefined
            }
        });
    });

    test('honors include options when set to a string', (test) => {
        return runTest(test, 'base/entry.ts', {
            'base/entry.ts': `export {foo} from "../foo";`,
            'foo.ts': `export const foo: number = 5;`,
            'tsconfig.json': JSON.stringify({
                compilerOptions: {
                    jsx: 'react',
                    target: 'ES2015'
                }
            })
        }, {
            options: {
                include: '.ts$'
            },
            expectation: {
                foo: 5
            }
        });
    });

    test('honors include options when set to a mix of string and regular expression', (test) => {
        return runTest(test, 'base/entry.ts', {
            'base/entry.ts': `export {foo} from "../foo";`,
            'foo.tsx': `export const foo: number = 5;`,
            'tsconfig.json': JSON.stringify({
                compilerOptions: {
                    jsx: 'react',
                    target: 'ES2015'
                }
            })
        }, {
            options: {
                include: [
                    '.ts$',
                    /\.tsx$/
                ]
            },
            expectation: {
                foo: 5
            }
        });
    });
});